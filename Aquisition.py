#
# Processing raw data to obtain SingleLook Complex image
#
# Purpose:  FOCUS SAR Simulator
# Author:   Nicolas Valentin Conde
# Creation Date:  June 2021
#

import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
from scipy.signal import chirp, spectrogram, butter, lfilter, freqz, remez

from GlobalVariables import FOCUS

def SaveRawDataFile():
	return 0

def W(s):
	"""
	Antenna pattern
	Wa(s) = sinc( (La / Lambda * arctan(s / R0) )^2
	La : Physical length of the antenna
	"""
	return np.sinc( 0.886 * FOCUS.La / FOCUS.Lambda * np.arcsin(-FOCUS.VArray*s/FOCUS.RArray)) ** 2

def sarResponse(Targets, t):
	"""
	Sar response for three targets.

	Args:
		- Targets: (list) [m] Range to target array
		- t: (float) [sec] fast time

	Outputs:
		- SAR Response
	"""
	SARResponse = 0
	for target in Targets:
		TargetPhase = -4j*np.pi*(FOCUS.RArray+target)/FOCUS.Lambda + np.pi*1j*FOCUS.K * (t - 2*((FOCUS.RArray+target) - FOCUS.R0)/FOCUS.C)**2
		SARResponse += np.exp(TargetPhase)
	noise = np.random.randn(len(FOCUS.RArray)) + 1j * np.random.randn(len(FOCUS.RArray))
	return FOCUS.rect( t/FOCUS.TPulse ) * W(FOCUS.s) * ( SARResponse + noise )

def RemezFilter(data):
	cutoff      = FOCUS.B/2             # Desired cutoff frequency, Hz
	trans_width = (FOCUS.FS/2-cutoff)/4  # Width of transition from pass band to stop band, Hz
	n_taps      = 2**7  # Size of the FIR filter.

	# Use Remez algorithm to design filter coefficients
	lpf = remez(n_taps, [0, cutoff, cutoff + trans_width, FOCUS.FS/2], [1,0], Hz=FOCUS.FS)
	dec_rate = int(FOCUS.FS/FOCUS.B)  #hago la decimacion con el factor que esta aqui
	
	# Filtro pasabajos para eliminar canales adyacentes
	DataFiltered = lfilter(lpf, 1.0, data)
	DataFiltered = DataFiltered[0::dec_rate]
	
	return DataFiltered

def MainAquisition():
	RawData = []
	for index, value in enumerate(FOCUS.t):
		RawData += [sarResponse([6000, -3000], value)]

	FOCUS.RawData = np.array(RawData).T

	FOCUS.IMean				= np.real(FOCUS.RawData).mean()
	FOCUS.QMean				= np.imag(FOCUS.RawData).mean()
	FOCUS.DopplerCentroid	= - 2.0 * FOCUS.VArray / FOCUS.Lambda * (FOCUS.VArray * FOCUS.s) / FOCUS.R0
	FOCUS.__ShowParams__()
	return FOCUS.RawData

if __name__ == "__main__":
	RawData = MainAquisition()
	FOCUS.Waterfall(RawData, "Raw Data Image")

	#chirp = np.exp(1j*np.pi*FOCUS.K/10*t**2)
	#plt.plot(np.real(chirp));plt.show()
