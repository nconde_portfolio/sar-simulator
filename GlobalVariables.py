#
# Input Parameters for radar
#
# Purpose:  FOCUS SAR Simulator
# Author:   Nicolas Valentin Conde
# Creation Date:  June 2021
#

import numpy as np
import matplotlib.pyplot as plt

MuEarth = 398600.4415e9         # [km^3/s^2]- Gravitational parameter
REqEarth= 6378*1e3              # [m]		- Equatorial Radius of Earth

class instrument_params:
	"""Input parameters from the SAR instrument"""
	def __init__(self):
		self.SCIdentification = "FOCUS Alpha 1"

		# GLOBAL CONSTANTS :
		self.C       = 299792458.0           # (float) [m/s]	- Light velocity
		self.BoltzCte= 1.38054e-23           # (float) [J/K]	- Constante de Boltzmann

		# GLOBAL INPUT VARIABLES :
		self.Freq    = 9.65e9               # (float) [Hz]		- Carrier frequency, SAOCOM
		self.Lambda  = self.C/self.Freq     # (float) [m]		- Carrier wavelength
		self.dr      = 2.0			     	# (float) [m]		- Desired range resolution
		self.dy      = 2.45					# (float) [m]		- Desired azimuth resolution
		self.Nchirp  = 1024					# (int) [px]        - Sample number in range direction
		self.Nsa     = 1024					# (int) [px]        - Sample number of the synthetic apeture length

		# GLOBAL CONSTRAINTS :
		self.Sigma0  = -17                  # (float) [dB/m^2]	- Minimum backscatter detectable (SAOCOM)

		# GLOBAL BASICS CALCULATION :
		self.La			= self.dy * 2														# (float) [m]		- Antenna length in azimuth (taken from STRIX)
		self.Lr			= 0.7																# (float) [m]		- Antenna length in range (taken from STRIX)
		self.B			= self.dr * self.C / 2												# (float) [Hz]		- Bandwidth
		self.FS			= 1.1 * self.B * 2													# (float) [Hz]		- Sampling rate (Oversampling factor * Nyquist)
		self.ABeamWidth = self.Lambda / self.La												# (float) [rad]		- Antenna beamwidth in azimuth
		self.RBeamWidth = self.Lambda / self.Lr												# (float) [rad]		- Antenna beamwidth in range
		self.TPulse		= 1.67e-06															# (float) [s]		- Range pulse length ~5km
		self.PRF        = 3467														        # (float) [Hz]		- Pulse Repeetition Frequency (taken from here STRIX)
		self.t          = np.arange(-1.1*self.Nchirp*0.5,1.1*self.Nchirp*0.5,1)/self.FS		# (np.array) [s]	- range/quick time
		self.s          = (np.arange(0,self.Nsa,1) - self.Nsa/2) / self.PRF					# (np.array) [s]	- azimuth/slow time
		self.RArray     = self.LoadOrbitData()[0]											# (np.array) [m]	- Range array
		self.VArray     = self.LoadOrbitData()[1]											# (np.array) m/s]	- Satellite velocity array
		self.HArray     = self.LoadOrbitData()[2]											# (np.array) [m]	- Orbit Height array
		self.LookAngles = self.LoadOrbitData()[3]											# (np.array) [rad]	- Look angles array
		self.R0         = self.RArray.min()													# (float) [m]		- Target range zero-Doppler
		self.H0         = self.HArray[np.argmin(self.RArray)]								# (float) [m]		- Orbit height zero-Doppler
		self.LookAngle0 = self.LookAngles[np.argmin(self.RArray)]							# (float) [rad]		- Target look angle zero-Doppler
		self.RNear      = self.H0 / np.cos(self.LookAngle0 - self.RBeamWidth/2)				# (float) [m]		- Radar minimum range target
		self.RFar       = self.H0 / np.cos(self.LookAngle0 + self.RBeamWidth/2)				# (float) [m]		- Radar maximum range target
		self.PRFmax		= 2*self.RNear/self.C												# (float) [Hz]		- Pulse repetition frequency
		self.K			= self.B / self.TPulse												# (float) [Hz/s]	- Chirp rate 
		self.CR			= self.K * self.TPulse**2											# (float) [-]		- Compression ratio - width input / width ouTPulseut
		self.Wr			= self.RBeamWidth * self.R0 / np.cos(self.LookAngle0)				# (float) [m]		- Swath in range direction
		self.Wa			= self.ABeamWidth * self.R0											# (float) [m]		- Swath in azimuth direction
		self.AveragePW  = self.RangeEquation()[0]
		self.PeakPW     = self.RangeEquation()[1]
		
		# DATA INFORMATION
		self.RawData			= None				# (np.array) [dB]	- RawData aquired
		self.IMean				= None				# (float) [dB]		- RawData Imaginary part mean value
		self.QMean				= None				# (float) [dB]		- RawData Real part mean value
		self.ICUStart			= None				# (float) [s]		- Spacecraft clock for first sample
		self.ClockStart			= None				# (float) [s]		- Clock Start
		self.ClockStop			= None				# (float) [s]		- Clock Stop
		self.DopplerCentroid	= None				# (float) [-]		- Doppler Centroid

	def RangeEquation(self):
		NoiseTemp = 500 # (float) [K] - System's noise temperature
		NoiseFigure = 10**(-8.9/10)
		AtmosphericLoss = 10**(-20/10)
		S0 = 10**(self.Sigma0/10)
		C1 = 2 * (4*np.pi)**3 * self.BoltzCte * NoiseTemp * self.La * self.Lr / 1.44
		G  = 4*np.pi * self.La * self.Lr * 0.6 / self.Lambda**2

		PAverage	= C1 * self.RArray**3 * self.VArray * NoiseFigure * AtmosphericLoss / (S0 * G**2 * self.Lambda**3 * self.dr)
		PPeak		= PAverage / self.TPulse / self.PRF
		return PAverage, PPeak

	def LoadOrbitData(self):
		Satellites = np.load('C:/Users/nicol/gncsim_basilisk/test_reports/5days_10sats_latlon.npy')
		target     = np.array([7356.,  111.4027243, 5929.918737, 2338.255123, 21.64824268, 88.92373649])
		OrbitIndex = np.where((Satellites[:,1]>2459573.5037615)*(Satellites[:,1]<2459573.50376158)*(Satellites[:,0]==1))[0][0]
		Satellites = Satellites[OrbitIndex-23:OrbitIndex+18] # R0 on the center of the array

		# Aconditioning time
		Satellites[:,1] -= Satellites[0,1]
		Satellites[:,1] *= 24*3600
		Satellites[:,1] -= Satellites[-1,1]/2

		# Distance calculation
		Satellite = np.zeros((len(Satellites),5))
		Satellite[:,0] = Satellites[:,1]

		for i in range(len(Satellites)):
			Satellite[i,1] = np.linalg.norm(Satellites[i,2:5]-target[1:4]*1e3)
			Satellite[i,2] = np.linalg.norm(Satellites[i,5:8])
			Satellite[i,3] = np.linalg.norm(Satellites[i,2:5])-REqEarth
			Satellite[i,4] = np.arccos( ((Satellites[i,2:5]-target[1:4]*1e3)/Satellite[i,1]) @ (Satellites[i,2:5]/np.linalg.norm(Satellites[i,2:5])) )  # angle between position and sat/target vectors
		
		R			= X(self.s, 1, Satellite)
		V			= X(self.s, 2, Satellite)
		H			= X(self.s, 3, Satellite)
		LookAngle	= X(self.s, 4, Satellite)

		return R, V, H, LookAngle

	def rect(self, x):
		"""
		Rectangle function:
			rect(x) = {1, if |x|<= 0.5; 0, otherwise}
		"""
		hs  = 0.5 * (np.sign(x+0.5) + 1.0)		# Heavyside function - hv(x) = {1, if x>=0; 0, otherwise}
		ihs = 0.5 * (1.0 - np.sign(x-0.5))		# Inverse Heavyside function - ihv(x) = {0, if x>=0; 1, otherwise}
		return hs * ihs

	def __ShowParams__(self):		
		print("---- FOCUS Metadata ----")
		print("\n\t---- FILE INFORMATION :")
		print("\t\t- Satellite Identification (m) :\t\t",				FOCUS.SCIdentification)
		print("\t\t- Raw Data Real numbers mean value (m) :\t",			FOCUS.IMean)
		print("\t\t- Raw Data Imaginary numbers mean value (m) :\t",	FOCUS.QMean)
		print("\t\t- Satellite Clock for firts sample (m) :\t",			FOCUS.ICUStart)
		print("\t\t- Satellite Start Clock Timing (m) :\t\t",			FOCUS.ClockStart)
		print("\t\t- Satellite Stop Clock Timing (m) :\t\t",			FOCUS.ClockStop)
		print("\t\t- Maximum Doppler Centroid (Hz) :\t\t\t",			FOCUS.DopplerCentroid.max())

		print("\n\t---- RADAR CHARACTERISTICS :")
		print("\t\t- Wavelength, Lambda (m) :\t\t\t",					FOCUS.Lambda)
		print("\t\t- Sample Rate, FS (MHz) :\t\t\t",					FOCUS.FS/1e6)
		print("\t\t- Bandwidth, B (MHz) :\t\t\t\t",						FOCUS.B/1e6)
		print("\t\t- Radar pulse repetition frequency, PRF (pps) :\t",	FOCUS.PRF)
		print("\t\t- Radar pulse time, TPulse (usec) :\t\t",			FOCUS.TPulse*1e6)
		print("\t\t- Chirp slope, K (GHz/s) :\t\t\t",					FOCUS.K/1e9)
		print("\t\t- Average Power (kW) :\t\t\t",						FOCUS.AveragePW/1e3)
		print("\t\t- Peak Power (kW) :\t\t\t\t",						FOCUS.PeakPW/1e3)

		print("\n\t---- ORBITAL INFORMATION :")
		print("\t\t- Range distance, R0 (km) :\t\t\t",					FOCUS.R0/1e3)
		print("\t\t- Near range distance, RNear (km) :\t\t",			FOCUS.RNear/1e3)
		print("\t\t- Mean Velocity, VSat (km/s) :\t\t\t",				FOCUS.VArray.mean()/1e3)
		print("\t\t- Elevation angle, Theta (rad) :\t\t",				FOCUS.LookAngle0*180/np.pi)
		print("\t\t- Antenna Swath in azimuth (km):\t\t",				FOCUS.Wa/1e3)
		print("\t\t- Antenna Swath in range (km):\t\t\t",				FOCUS.Wr/1e3)

		print("\n\n")
		return 0

	def Waterfall(self, data, title):
		"""
		Waterfall plotting function

		Args:
			- data: SAR data
			- title: Plot title
		"""

		RMin = FOCUS.R0-len(data[0])/2/FOCUS.FS*FOCUS.C
		RMax = FOCUS.R0+len(data[0])/2/FOCUS.FS*FOCUS.C

		AbsData = np.absolute(data) / np.absolute(data).max()

		f, axs = plt.subplots(2,1, figsize=(8,12), sharex=True, sharey=True)
		im2=axs[0].imshow(np.angle(data), vmin=-np.pi, vmax=np.pi, aspect='auto', extent=[FOCUS.RNear, FOCUS.RFar, 0, len(data)*FOCUS.VArray.mean()/FOCUS.PRF]); axs[0].set_title(title + ' - Phase')	
		im1=axs[1].imshow(AbsData, aspect='auto', extent=[FOCUS.RNear, FOCUS.RFar, 0, len(data)*FOCUS.VArray.mean()/FOCUS.PRF]); axs[1].set_title(title + ' - Magnitude')
	
		plt.xticks(rotation=45)
		plt.setp(axs[-1], xlabel='Range [m]')
		plt.setp(axs, ylabel='Azimuth [m]')
		f.colorbar(im1, ax=axs[1])
		f.colorbar(im2, ax=axs[0])
		plt.show();
	
		return 0

	def PRFLimits(self, h, lookangle, RNear, RFar, j):
		PRFMin1    = j / ( 2 * RNear / FOCUS.C - FOCUS.TPulse - 2 * h / FOCUS.C )
		PRFMin2    = j / ( 2 * RNear / FOCUS.C - FOCUS.TPulse )
		PRFMax1    = (j + 1) / ( 2 * RFar / FOCUS.C + FOCUS.TPulse - 2 * h / FOCUS.C )
		PRFMax2    = (j + 1) / ( 2 * RFar / FOCUS.C + FOCUS.TPulse )
		PRFMaximum = 1 / ( 2 * FOCUS.TPulse + 2 * ( RFar - RNear ) / FOCUS.C )
		PRFMinimum = np.array([2 * FOCUS.VArray.min() / FOCUS.La] * len(RFar))
		return PRFMinimum, PRFMin1, PRFMin2, PRFMax1, PRFMax2, PRFMaximum

	def ZebraMap(self):
		lookangle= np.linspace(20, 60, 50)*np.pi/180
		RNear    = FOCUS.H0 / np.cos(lookangle - FOCUS.RBeamWidth/2)				# [m]   - Radar minimum range target
		RFar     = FOCUS.H0 / np.cos(lookangle + FOCUS.RBeamWidth/2)				# [m]   - Radar maximum range target

		plt.figure(figsize=(10,6))

		PRFList = []
		for j in range(1,50,1):
			PRFMinimum, PRFMin1, PRFMin2, PRFMax1, PRFMax2, PRFMaximum = FOCUS.PRFLimits(FOCUS.H0, lookangle, RNear, RFar, j)

			plt.fill_betweenx(lookangle*180/np.pi, PRFMax1, PRFMin1, color='#5a8ab4', alpha=0.3)
			plt.fill_betweenx(lookangle*180/np.pi, PRFMax2, PRFMin2, color='#285f90', alpha=0.3)
			plt.plot(PRFMinimum,lookangle*180/np.pi,color='#daebc1',linewidth=1)
			plt.plot(PRFMaximum,lookangle*180/np.pi,color='#daebc1',linewidth=1)

			PRFList += [PRFMinimum, PRFMin1, PRFMin2, PRFMax1, PRFMax2, PRFMaximum]

		plt.xlim([PRFMinimum[0],3800])
		plt.ylim([30,50])
		plt.xlabel('PRF [pps]')
		plt.ylabel('Ángulo de incidencia [deg]')
		plt.title('Banda X - Ángulo de Incidencia vs PRF')
		plt.show()

		return 0

	def SideLookingGeometryTiming(self, LookAngle):
		"""
		Side Looking Geometry and Timing

		Args:
			Oi:  [deg] - Look angle (Typical 20 - 60 deg)
			h:   [m]   - Orbit height
			PRF: [Hz]  - Pulse repetition frequency

		Outputs:
			td: Time  delay for received signal
			SWmax: Maximum swath width
			r0: Slant range
		"""
		r0 = FOCUS.H0 / np.cos(np.deg2rad(LookAngle))                         # Aproximation - valid only for flat terrain 
		td = 2 * r0 / FOCUS.C
		SWmax = FOCUS.C / 2 / FOCUS.PRF / np.sin(np.deg2rad(LookAngle))
		return td, SWmax, r0

def X(s, i, Satellite):
	"""
	Array for integration time, calculated with linear interpolation.
	Could be Orbit Height, Range from satellite to target or satellite velocity

	Args:
		- s:    slow time array
		- i:    (int) 1=Range, 2=Velocity, 3=Height
		- Satellite:    (numpy array)

	Outputs:
		- X(s) choosed array
	"""
	X = []
	for S in s:
		indx = np.where(Satellite[:,0]<S)[0]
		if indx.sum() == 0:
			indx = 0
		elif (Satellite[:,0]<S).sum()>=len(Satellite)-2:
			indx = len(Satellite)-2
		else:
			indx = indx[-1]

		X1   = Satellite[indx,i]
		X2   = Satellite[indx+1,i]
		X   += [(X2-X1)/0.5 * (S-Satellite[indx,0]) + X1]
	return np.array(X)

FOCUS = instrument_params()

if __name__ == "__main__":
	print(FOCUS.AveragePW)	
	print(FOCUS.PeakPW)	
	#FOCUS.ZebraMap()