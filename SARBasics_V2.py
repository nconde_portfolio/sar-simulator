# -*- coding: utf-8 -*-
#
# SAR BAsics Calculations
#
# Purpose:  FOCUS SAR Calculator
# Author:   Nicolas Valentin Conde
# Creation Date:  June 2021
#

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import pandas as pd
from tabulate import tabulate
from labellines import labelLine, labelLines
import sys

##### Auxiliary Functions

def normalize(x):
    return x / sum(x)

def progressbar(it, size=60, out=sys.stdout): # Python3.3+
    count = len(it)
    def show(j):
        x = int(size*j/count)
        print("{}[{}{}] {}/{}".format("Computing: ", u"█"*x, "."*(size-x), j, count), end='\r', file=out, flush=True)
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    print("\n", flush=True, file=out)

##### Models

def Power(sar, Larr, Lr, lookangle, f, S0, lsys): # Link budget para calcular la potencia
    s0 = 10**(S0/10) # Sigma 0 de dB a veces
    R = sar.h / np.cos(lookangle) # Radio hasta el target segun altura orbital y lookangle
    Tsys = 290 # Temperatura de ruido - Standard cuando se desconoce
    Lsys = 10**(lsys/10)  # Pérdida del sistema - Strix
    Nf = 4 # Figura de ruido - Strix
    n = 0.5 # Eficiencia de la antena
    G = n * 4 * np.pi * Larr * Lr / (sar.c/f)**2
    SNR = 1
    # calculo de potencia promedio
    P_average = SNR*256*np.pi**3*R**3*sar.Kb*Tsys*sar.B*Nf*Lsys*sar.V*np.sin(lookangle)/(sar.c/f)**3/sar.c/s0/(G)**2
    return P_average

def Data(sar, La, Lr, PRF, lookangle, f, Tp): # calculo de cantidad de datos generados
    R  = sar.h / np.cos(lookangle) # Radio hasta el target segun altura orbital y lookangle
    Wr = sar.c/f / Lr * R # pisada del radar en rango (es el ancho del pad "iluminado" por el haz de la antena)
    obs_t = 2*Wr/sar.c + Tp # Observation time
    Of = 1.05 # Oversampling factor - factor de diseño, puede variar, lo saque de un libro el valor
    Nb = 5 # bits/sample - Quantization - tambien sacado de un libro
    fsample = Of * 2 * sar.B  # data rate - 2 veces el ancho de banda (nyquist) * oversampling factor
    Nsamples = fsample * obs_t # tiempo de observacion * sample rate
    rsample = Nb * fsample # instantaneous data rate
    DL = obs_t * rsample * PRF / 1e6 # Mbps - Real Time downlink - el downlink q necesito para bajar los datos en tiempo real
    return DL

def groundresolution(sar): # Curvas de nivel de lookangle vs ancho de banda para conseguir varias resoluciones en tierra
    B = np.linspace(10e6, 500e6, 500)
    dg = np.array([10, 5, 2, 1, 0.8, 0.6])
    plt.figure(figsize=(5,6))
    for g in dg:
        arg_arcsin = sar.c / 2 / B / g
        # Filtro para +-1 por el dominio del arc sin, y el 0.8 (~45°) es para que el grafico se vea bien
        theta = np.arcsin(arg_arcsin[(abs(arg_arcsin)<0.8)])
        plt.plot(B[(abs(arg_arcsin)<0.8)]/1e6, theta*180/np.pi,label=str(g)+" m", c="#285f90")
    labelLines(plt.gca().get_lines(),align=False,fontsize=12, c="#285f90")
    plt.ylim([8,45]); plt.xlim([1,500])
    plt.xlabel("Ancho de banda [MHz]")
    plt.ylabel("Ángulo de vista [deg]")
    plt.show()

def azimuth_resolution_Spotlight(sar): # resolucion en azimuth para el modo spotlight
    plt.figure(figsize=(5,6))
    for F in [2,5,10,14]: # GHz
        dO = np.linspace(0.01, 0.07, 100)
        if F == 2:
            dO = np.linspace(0.025, 0.07, 100)
        dT = sar.h*dO/sar.V
        az = sar.c/F/1e9/2/dO
        plt.plot(dT, az, label=str(int(F))+" GHz", c="#285f90")
    labelLines(plt.gca().get_lines(),align=False,fontsize=12, c="#285f90")
    plt.xlabel("Tiempo de observación [sec]")
    plt.ylabel("Resolución acimut [m]")
    plt.ylim([0,3]); plt.xlim([0.85,5])
    plt.show()

def Diamond_Map(sar):
    f = 9.65e9 # Hz - frecuencia banda X
    fku = 13.4e9 # Hz - frecuencia banda Ku
    fka = 35.75e9 # Hz - frecuencia banda Ka
    Dr = 0.6
    Da = 4

    lamda = sar.c / f
    beta = np.arange(15,50,1)*np.pi/180
    # beta = 55*np.pi/180
    theta = lamda / Da

    tp = 4.9e-5

    Min = np.ones(len(beta))*2*sar.V/Da
    Max = sar.Maxim(beta, theta, tp)

    plt.figure(figsize=(10,6))

    for j in range(1,500,1):
        prfMaxold  = sar.prfmax(beta, theta, tp, j-1)
        prfMax2old = sar.prfmax2(beta, theta, tp, j-1)
        prfMin     = sar.prfmin(beta, theta, tp, j)
        prfMax     = sar.prfmax(beta, theta, tp, j)
        prfMin2    = sar.prfmin2(beta, theta, tp, j)
        prfMax2    = sar.prfmax2(beta, theta, tp, j)
        plt.fill_between(x = beta*180/np.pi, y1 = prfMin, y2 = prfMaxold, color='#285f90', alpha=0.5)
        plt.fill_between(x = beta*180/np.pi, y1 = prfMin2, y2 = prfMax2old, color='#285f90', alpha=0.5)
    
    plt.ylim([3300,4500])
    plt.ylabel('PRF [pps]')
    plt.xlabel('Ángulo de incidencia [deg]')
    plt.title('Banda X - PRF vs Ángulo de Incidencia')
    plt.show()

##### Simulator

def Montecarlo(sar, num_reps, num_simulations):
    # Selecciona num_reps valores de cada input y calcula la potencia, datos y área. Plotea un histograna.
    # Repite el procedimiento para num_simulations.    
    for i in progressbar(range(num_simulations),num_simulations): # Loop through many simulations
        # Choose random inputs
        La_W        = np.random.choice(sar.La, num_reps)
        Lr_W        = np.random.choice(sar.Lr, num_reps)
        PRF_W       = np.random.choice(sar.PRF, num_reps)
        S0_W        = np.random.choice(sar.S0, num_reps)
        f_W         = np.random.choice(sar.f, num_reps, p=normalize(signal.gaussian(len(sar.f), std=3)))
        Tp_W        = np.random.choice(sar.Tp, num_reps)
        lsys_W      = np.random.choice(sar.lsys, num_reps)

        history_lookangle = []
        lookangle_W = np.array([])
        for u in range(len(Lr_W)):
            LOKKAA = np.linspace(sar.lookangle[0],sar.lookangle[1],int((sar.lookangle[1]-sar.lookangle[0])/sar.c*f_W[u]*Lr_W[u]))
            lookangle_W = np.append(lookangle_W, np.random.choice(LOKKAA, 1))
            history_lookangle += [LOKKAA]
        
        # to look distribution
        #plt.hist(La_W, bins = 200)
        #plt.show()

        # Build the dataframe based on the inputs and number of reps
        df = pd.DataFrame(index=range(num_reps), data={'La': La_W, 'Lr': Lr_W, 'PRF': PRF_W, 'init_lookangle':lookangle_W*180/np.pi, 'lookangle':lookangle_W, 'Sigma0':S0_W, 'f':f_W, 'Tp%':Tp_W, 'lsys':lsys_W})
        
        initial_df = df["init_lookangle"]

        if i == 0:
            corr_init = df.corr(method ='spearman').round(2)

        # Applying filters
        df = df[df.apply(lambda x : sar.Amin(x["lookangle"], x["f"], x["La"], x["Lr"]), axis=1)>0]
        df = df[df.apply(lambda x : sar.PRFmin(x["PRF"], x["La"]), axis=1)>0]
        df = df[df.apply(lambda x : sar.Wrmax(x["lookangle"], x["La"]), axis=1)>0]
        df = df[df.apply(lambda x : sar.PRFmax(x["lookangle"], x["Lr"], x["PRF"], x["f"]), axis=1)>0]
        
        df = df.dropna()

        # Determine Power consumption
        df['Pavg'] = df.apply(lambda x : Power(sar, x["La"], x["Lr"], x["lookangle"], x["f"], x["Sigma0"], x["lsys"]), axis=1)
        df['Data'] = df.apply(lambda x : Data(sar, x["La"], x["Lr"], x["PRF"], x["lookangle"], x["f"], x["Tp%"]/x["PRF"]), axis=1)
        df['Aant'] = df["La"] * df["Lr"]
        df['Pp'] = df['Pavg']/df['Tp%']
        
        #df = df[df.apply(lambda x : sar.ambiguity(x["lookangle"], x["La"], x["Lr"], x["PRF"], x["Tp%"], x["f"]), axis=1)>0]
        
        df = df[df['Pp'] < 3500] # Applying filters
        df["lookangle"] = df["lookangle"]*180/np.pi # Cambio de unidad (rad -> deg)
        df["Tp"] = df["Tp%"]/df["PRF"]*10e6 # Cambio de unidad (rad -> deg)

        if i == 0:
            fig, ax = plt.subplots(2, 3,figsize=(12,10)) # ax = ((ax1, ax2, ax3), (ax4, ax5, ax6))
            selected_columns = [[df["Pavg"], df["Pp"]], [df["Data"]], [df["Aant"]], [df["lookangle"],initial_df], [df["Tp"]], [df["PRF"]]]
        elif i == 6:
            selected_columns = [[df["Pavg"], df["Pp"]], [df["Data"]], [df["Aant"]], [df["lookangle"]], [df["Tp"]], [df["PRF"]]]


        for row in range(len(ax)):
            for col in range(len(ax[row])):
                select = int(2*row+row+col)
                c = "#285f90"
                for dataF in selected_columns[select]:
                    ax[row][col].hist(dataF, bins = 100, alpha=0.6/num_simulations, color=c, label=dataF.name);
                    c = "#74a65e"
                if i == 0:
                    ax[row][col].legend(loc="upper right")
                    

        if i == 0:
            correlation = df.corr(method ='spearman')/num_simulations
        else:
            correlation += df.corr(method ='spearman')/num_simulations

    titles = ['(a) Distribución de potencia promedio y pico', '(b) Distribución de datos generados',
              '(c) Distribución de área de antena', '(d) Distribución de ángulo de vista',
              '(e) Distribución del tiempo de pulso', '(f) Distribución del PRF'];

    xlabels = ['Potencia [W]', 'data [Mbps]', '$A_{ant}$ [$m^2$]', '$ \Theta $ [deg]', '$T_P$ [$\mu$s]', "PRF [1/s]"];
    
    for row in range(len(ax)):
        for col in range(len(ax[row])):
            select = int(2*row+row+col)
            ax[row][col].set_title(titles[select]);
            ax[row][col].set_xlabel(xlabels[select]);

    ax[0][0].set_ylabel("N° de Repeticiones");
    ax[1][0].set_ylabel("N° de Repeticiones");
    plt.subplots_adjust(hspace=0.3)
    #plt.show()

    plt.figure(figsize=(10,6))
    sar.Diamond_Map(f_W.mean(), Lr_W.max(), np.linspace(history_lookangle[-1].min(), history_lookangle[-1].max(), 500), Tp_W.mean()/PRF_W.mean(), [3000,7000])
    for list_la in history_lookangle[-1]:
        plt.axvline (x=list_la*180/np.pi);
    plt.show()

    # Tablas
    print("Initial orrelation table\n")
    print(tabulate(corr_init, headers='keys', tablefmt='psql'))
    print("\n")

    print("Final Correlation table\n")
    print(tabulate(correlation.iloc[:,8:].round(2), headers='keys', tablefmt='psql'))
    print("\n")
    #correlation.to_csv('C:/Users/nicol/OneDrive/Documents/FOCUS_SAR_SIMULATOR/figs/raw_data.csv', index=False)

class Satellite():
    ##### INPUTS
    La  = np.linspace(3, 5, 50) # [m] antena en azimuth
    Lr  = np.linspace(.4, .8, 50) # [m] antena en rango
    PRF = np.linspace(3000,8000,50) # [1/sec] pulse repetition frequency
    lookangle = np.array([15,45])*np.pi/180 # [deg] angulo de vista respecto al plano orbital
    S0  = np.linspace(-18,-17,50) # [dB/m2] sigma 0, scatterer detectable por el radar. -22 tiene SAOCOM
    f   = np.linspace(9,10,50)*1e9 # [Hz] frecuencia central
    Tp  = np.linspace(0.1,0.25,50) # [%] tiempo de pulso
    lsys  = np.linspace(2,5,50) # [dB] Pérdida de potencia rf en el sistema

    ##### Requirements
    dy_max = 1.0                    # (float) m - Spotlight, Desired azimuth resolution - Menor a
    dr_max = 1.0                    # (float) m - Spotlight, Desired range resolution - Menor a
    Wa_max = 5.0e3                  # (float) km - Spotlight - Menor a
    Wr_max = 5.0e3                  # (float) km - Spotlight - Menor a

    ##### Constants
    #Lambda  = c/f                  # (float) [m] - Carrier wavelength
    h       = 560e3                 # (float) [m] - Orbital height
    MuEarth = 398600.4415e9         # [km^3/s^2]- Gravitational parameter
    REqEarth= 6378*1e3              # [m]		- Equatorial Radius of Earth
    c       = 299792458.0           # (float) [m/s]	- Light velocity
    Kb      = 1.38054e-23           # (float) [J/K]	- Constante de Boltzmann
    V       = (MuEarth/(h+REqEarth))**.5
    B       = c / (2 * dr_max)      # Bandidth

    ##### Constraints
    Amin   = lambda self, lookangle, f, La, Lr: La*Lr > 4*self.V*np.tan(lookangle)/f # restriccion de area
    PRFmin = lambda self, PRF, La: PRF > 2 * self.V / La # restriccion de PRF minimo
    PRFmax = lambda self, lookangle, Lr, PRF, f: PRF < 1 / ( 2 * self.c/f / Lr * self.h / np.cos(lookangle) / self.c ) # restriccion de PRF maximo
    Wrmax  = lambda self, lookangle, La: self.c * La / lookangle / 4 / self.V # Slant range máximo para evitar ambigüedades (dado por el PRF minimo
    Pmax   = lambda self, Ps: Ps < 2000 # restriccion puesta a dedo para la potencia pico

    def ambiguity(self, lookangle, La, Lr, PRF, tp, f):
        lamda = sar.c / f
        theta = lamda / La

        prfMin = self.prfmin(lookangle, theta, tp/PRF, 1)
        if PRF > prfMin:
            j = int(PRF / abs(prfMin))
            prfMax     = self.prfmax(lookangle, theta, tp/PRF, j)
            prfMin2    = self.prfmin2(lookangle, theta, tp/PRF, j)
            prfMax2    = self.prfmax2(lookangle, theta, tp/PRF, j)
            return (PRF < prfMax) or (PRF > prfMin2) and (PRF<prfMax2)
        else:
            return False
    
    def Maxim(self, beta, theta, tp):
        Rf = self.h/np.cos(beta+theta/2)
        Rn = self.h/np.cos(beta-theta/2)
        prf = 1/(2*tp+2*(Rf-Rn)/self.c)
        return prf

    def prfmin(self, beta, theta, tp, j):
        Rn = self.h/np.cos(beta-theta/2)
        prf = j/(2*Rn/self.c-tp-2*self.h/self.c)
        return prf

    def prfmax(self, beta, theta, tp, j):
        Rf = self.h/np.cos(beta+theta/2)
        prf = (j+1)/(2*Rf/self.c+tp-2*self.h/self.c)
        return prf

    def prfmin2(self, beta, theta, tp, j):
        Rn = self.h/np.cos(beta-theta/2)
        prf = j/(2*Rn/self.c-tp)
        return prf

    def prfmax2(self, beta, theta, tp, j):
        Rf = self.h/np.cos(beta+theta/2)
        prf = (j+1)/(2*Rf/self.c+tp)
        return prf
    
    def Diamond_Map(self, f, Dr, beta, tp, ylimit):
        lamda = self.c / f
        theta = lamda / Dr

        for j in range(1,500,1):
            prfMaxold  = self.prfmax(beta, theta, tp, j-1)
            prfMax2old = self.prfmax2(beta, theta, tp, j-1)
            prfMin     = self.prfmin(beta, theta, tp, j)
            prfMin2    = self.prfmin2(beta, theta, tp, j)
            plt.fill_between(x = beta*180/np.pi, y1 = prfMin, y2 = prfMaxold, color='#285f90', alpha=0.5)
            plt.fill_between(x = beta*180/np.pi, y1 = prfMin2, y2 = prfMax2old, color='#285f90', alpha=0.5)
        
        plt.ylim(ylimit)
        plt.ylabel('PRF [pps]')
        plt.xlabel('Ángulo de incidencia [deg]')
        plt.title('Banda X - PRF vs Ángulo de Incidencia')

    
if __name__ == "__main__":
    sar = Satellite()
    
    #### Numeros para manejar el montecarlo
    num_reps = 10000 # numero de valores random seleccionados por simulacion
    num_simulations = 2 # numero de simulaciones

    df = Montecarlo(sar, num_reps, num_simulations)
    #groundresolution(sar)
    #azimuth_resolution_Spotlight(sar)
    #Diamond_Map(sar)
    """
    Amin   = lambda lookangle, f: 4*V*np.tan(lookangle)/f

    freq = np.array([2e9, 5e9, 9.65e9])

    plt.figure(figsize=(5,6))
    for f1 in freq:
        plt.plot(lookangle, Amin(lookangle, f1))
    plt.xlabel("Ángulo de vista [deg]")
    plt.ylabel("Área mínima [m2]")
    plt.title("Área mínima para distintas frecuencias")
    plt.legend(["Banda L", "Banda C", "Banda X"])
    plt.show()
    """