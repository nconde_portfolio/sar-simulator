#
# Processing raw data to obtain SingleLook Complex image
#
# Purpose:  FOCUS SAR Simulator
# Author:   Nicolas Valentin Conde
# Creation Date:  June 2021
#
# Referencias:
#	- [1] Inverse Synthetic Aperture Radar Imaging With MATLAB Algorithms
#	- [2] Digital Processing of Synthetic Aperture Radar Data Algorithms and Implementation
#	- [3] SAR IMAGE FORMATION: ERS SAR PROCESSOR CODED IN MATLAB
#

import numpy as np
from numpy.matlib import repmat
import matplotlib.pyplot as plt
import scipy.ndimage
from scipy import interpolate

from GlobalVariables import FOCUS
from Aquisition import MainAquisition

def LoadRawData(FileName):
	"""
	Load data output from aquisition code

	Args:
		- FileName

	Outputs:
		- RawData
	"""
	return MainAquisition()

def RangeCompression(data, t):
	"""
	Focussing data with tx chirp in range direction. Matched filter.

	Args:
		- data: raw data
		- t:    fast time array

	Outputs:
		- RangeCompressed - data compressed in range
	"""
	RangeReference = FOCUS.rect(t/t.max()) * np.exp(-1j * np.pi * FOCUS.K * t**2)	 # conjugated chirp for mixer
	
	RangeCompressed = []
	for DATA in data:
		RangeCompressed += [ FOCUS.rect(t/t.max()) * scipy.ndimage.convolve1d(DATA, RangeReference, mode='constant')]

	return np.array(RangeCompressed)

def RangeMigration(data, s):
	"""
	Doppler displacement correction. RCMC algorithm with interpolation.

	Args:
		- data: data focussed with tx chirp
		- s:    slow time array
		- R:    range at each slow time array
		- V:    satellite velocity at each slow time array

	Outputs:
		- RangeMigrated - range migrated data
	"""
	deltaCell = np.abs(FOCUS.RArray * (FOCUS.TPulse * FOCUS.Nsa * FOCUS.VArray / FOCUS.RArray)**2 / (8 * 2 * FOCUS.B / FOCUS.C)) # Eq. 3.64 - [1]
	RangeMigrated = []
	
	for i in range(data.shape[0]):
		RangeMigrated += [np.roll(data[i], int(deltaCell[i])+1)]

	return np.array(RangeMigrated)

def AzimuthCompression(data, s):
	"""
	Focus image in azimuth

	Args:
		- data: range migrated data in frequency domain
		- s: slow time array
		- R:    range at each slow time array
		- V:    satellite velocity at each slow time array

	Outputs:
		- AzimuthCompressed - range migrated data focussed in azimuth
	"""

	# First attempt - Ref [3]
	#exp1 = np.exp(-4j*FOCUS.R0**2/FOCUS.Lambda)
	#exp2 = np.exp(-1j*np.pi*(2 * Ka * (R-FOCUS.R0) * V * s + Ka * (V*s)**2))
	#AzimuthReference = FOCUS.rect( (R-FOCUS.R0) / (2.0*(R[-1]-R[0])) ) * np.exp(4j*np.pi*R/FOCUS.Lambda)
	
	# Second attempt - Ref [2]
	#Ka = 2.0 * V**2 / FOCUS.Lambda / FOCUS.R0
	#AzimuthReference = np.exp( -1j * np.pi * Ka * s**2 )

	## Third attempt - Ref [1] - Eq. 3.49
	#Ka = 2.0 / FOCUS.Lambda / FOCUS.R0
	#AzimuthReference = FOCUS.rect(Ka * V**2 * FOCUS.Tp * FOCUS.Nsa * s) * np.exp( 1j * np.pi * Ka * (V*s)**2 )

	# Fourth attempt 
	Ka = 2.0 / FOCUS.Lambda / FOCUS.R0
	AzimuthReference = np.kaiser(len(s), 14) * np.exp( 1j * np.pi * Ka * (FOCUS.RArray-FOCUS.R0)**2 )

	Nasa = len(s) + data.shape[0] - 1
	Nsa2 = Nasa if Nasa % 2 == 0 else Nasa+1

	SrxMF=np.zeros((Nsa2, data.shape[1]),dtype=complex)
	hMF = np.pad(AzimuthReference, [int(np.floor((Nsa2-len(data[0]))*0.5)), int(np.ceil((Nsa2-len(data[0]))*0.5))])
	
	AzimuthCompressed = []
	for i in range(0,len(data[0])):
		colMF = np.pad(data[:,i], [int(np.floor((Nsa2-len(data[:,i]))*0.5)), int(np.ceil((Nsa2-len(data[:,i]))*0.5))])
		AzimuthCompressed += [scipy.ndimage.convolve1d(colMF, hMF, mode='constant')]
	AzimuthCompressed = np.array(AzimuthCompressed).T

	return AzimuthCompressed

def MainProcessing():
	RawData = LoadRawData("Hola")
	print("---- Raw Data loaded\n")
	#FOCUS.Waterfall(RawData, "Raw Data Image")
	
	RangeCompressed = RangeCompression(RawData, FOCUS.t)
	print("---- Range compressed\n")
	FOCUS.Waterfall(RangeCompressed,"Range Compressed Image")

	AzimuthFFT = np.fft.fft2(RangeCompressed,axes=[0])
	print("---- Azimuth FFT\n")
	#FOCUS.Waterfall(AzimuthFFT, "Azimuth FFT Image")

	RangeMigrated = RangeMigration(AzimuthFFT, FOCUS.s)
	print("---- Range migrated\n")
	FOCUS.Waterfall(RangeMigrated, "Range Migrated Image")

	AzimuthCompressed = AzimuthCompression(RangeMigrated, FOCUS.s)
	print("---- Azimuth compressed\n")
	#FOCUS.Waterfall(AzimuthCompressed, "Azimuth Compressed Image")

	SLC = np.fft.fftshift(np.fft.ifft2(AzimuthCompressed,axes=[0]),axes=[0])
	FOCUS.Waterfall(SLC, "SingleLook Complex Image")

if __name__ == "__main__":
	MainProcessing()